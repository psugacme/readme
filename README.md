# PSUG ACME
## What is ACME?
ACME stands for Advanced Customizations Made for Everyone.  It is a distributed collaborative chartered in May 2018 to support the PowerSchool developer community.  Its primary functions are to discover useful technologies, share knowledge, and build PowerSchool customizations that are freely available to schools.
## Administrators 
- Adam Larsen, Community Unit School District 220 -- Oregon, IL <alarsen@ocusd.net>
- Jim Parsons, Valley Christian Schools -- Cerritos, CA <jparsons@vcschools.org>
- Eric Schaitel, Marcia Brenner Associates -- Madison, WI <eschaitel@mba-link.com>
- Jason Treadwell, Bright Arrow Technologies <jason@brightarrow.com>
## Membership
Membership in the collaborative is free and is available to anyone in the PowerSchool developer community.  Interested parties should complete the [signup form](https://goo.gl/forms/Q0ObnfYZt35TWIol2).  An approved member will be invited to the private Bitbucket team and Slack channel.  This will provide read and pull request access to all of the code repositories.  Group admins will manage code repositories.

## Contributing
* [BitBucket Git Workflow for ACME](https://docs.google.com/document/d/13gHlM_fU93iwBz43g_kYu8-qvDImv-riF_DDOxDnjVk/edit?usp=sharing)
A Google Doc with detailed steps on how to fork an ACME project, and submit a Pull Request if you have code to contribute.
* [GIT for PSUG ACME video lesson](https://drive.google.com/file/d/1kiK1vWX2aloaIy6kQGmjOMg-9Ax7oLeo/view?usp=share_link)
A 19-minute video lesson on the Fork and Pull collaboration model for PSUG ACME.